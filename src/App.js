
import './App.css';
import DataTable from './data-table';

function App() {
  return (
    <div>
      <DataTable/>
    </div>
  );
}

export default App;
