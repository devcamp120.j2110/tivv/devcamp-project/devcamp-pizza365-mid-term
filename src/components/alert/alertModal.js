import { Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions, Button } from '@mui/material'

function AlertModal({openAlertModal, setOpenAlertModal, message}) {
    const handleClose = () => {
        setOpenAlertModal(false);
    };
    return (
        <Dialog
            open={openAlertModal}
            onClose={handleClose}
        >
            <DialogTitle>
                Thông báo
            </DialogTitle>
            <DialogContent>
                <DialogContentText>
                    {message}
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button onClick={handleClose}>OK</Button>
            </DialogActions>
        </Dialog>
    )
}
export default AlertModal;