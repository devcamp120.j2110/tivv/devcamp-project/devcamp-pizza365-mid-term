import { Modal, Box } from '@mui/material';
import { useState } from 'react';
import { ModalHeader, ModalBody, ModalFooter, Button, Input, Col, Row, Label } from 'reactstrap'
import AlertModal from '../alert/alertModal';
const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 800,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};
function UpdateModal({ openModal, setOpenModal, data }) {
    //Đóng Modal
    const handleClose = () => {
        setOpenModal(false);
        window.location.reload();
    }
    //Hàm gọi API update đơn hàng
    const createOrder = async (paramUrl, paramOption = {}) => {
        const Fetch = await fetch(paramUrl, paramOption);
        const response = await Fetch.json();
        return response;
    }
    //Khai báo biến lưu trữ thông tin 
    const [trangThai, setTrangThai] = useState("");
    //Biến lưu trạng thái đóng, mở modal alert
    const [alertModal, setAlertModal] = useState(false);
    //Biến lưu thông tin hiển thị lên Popup
    const [message, setMessage] = useState("");
    
    //lưu dữ liệu vào biến khi chọn trạng thái
    const onStatusChange = (event) => {
        setTrangThai(event.target.value);
    }
    //Hàm xử lý nút xác nhận đặt hàng
    const onBtnConfirmClk = () => {
            createOrder("http://42.115.221.44:8080/devcamp-pizza365/orders/" + data.id, {
                method: 'PUT',
                body: JSON.stringify({
                    trangThai: trangThai
                }),
                headers: {
                    'Content-type': 'application/json; charset=UTF-8'
                }
            })
                .then(() => {
                    setMessage("Cập nhật đơn hàng thành công!");
                    setAlertModal(true);
                })
                .catch(() => {
                    setMessage("Cập nhật đơn hàng bị lỗi!");
                    setAlertModal(true);
                });
    }

    return (
        <div>
            <Modal
                open={openModal}
                onClose={handleClose}
            >
                <Box sx={style}>
                    <ModalHeader className='pt-0'>
                        Order Information
                    </ModalHeader>
                    <ModalBody>
                        <Row>
                            <Col xs='6'>
                                <Row className='form-group'>
                                    <Col xs='4' className='p-2'>
                                        <Label>Kích cỡ: <span>(*)</span></Label>
                                    </Col>
                                    <Col xs='8'>
                                        <select className='form-control'>
                                            <option>{data.kichCo}</option>
                                        </select>
                                    </Col>
                                </Row>
                            </Col>
                            <Col xs='6'>
                                <Row className='form-group'>
                                    <Col xs='4' className='p-2'>
                                        <Label>Nước uống: <span>(*)</span></Label>
                                    </Col>
                                    <Col xs='8'>
                                        <select className='form-control'>
                                            <option >{data.idLoaiNuocUong}</option>
                                        </select>
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                        <Row>
                            <Col xs='6'>
                                <Row className='form-group'>
                                    <Col xs='4' className='p-2'>
                                        <Label>Đường kính: </Label>
                                    </Col>
                                    <Col xs='8'>
                                        <Input className='form-control' disabled value={data.duongKinh}></Input>
                                    </Col>
                                </Row>
                            </Col>
                            <Col xs='6'>
                                <Row className='form-group'>
                                    <Col xs='4' className='p-2'>
                                        <Label>Họ tên: <span>(*)</span></Label>
                                    </Col>
                                    <Col xs='8'>
                                        <Input className='form-control' value={data.hoTen}></Input>
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                        <Row>
                            <Col xs='6'>
                                <Row className='form-group'>
                                    <Col xs='4' className='p-2'>
                                        <Label>Sườn nướng: </Label>
                                    </Col>
                                    <Col xs='8'>
                                        <Input className='form-control'  disabled defaultValue={data.suon}></Input>
                                    </Col>
                                </Row>
                            </Col>
                            <Col xs='6'>
                                <Row className='form-group'>
                                    <Col xs='4' className='p-2'>
                                        <Label>Email:</Label>
                                    </Col>
                                    <Col xs='8'>
                                        <Input className='form-control' value={data.email}></Input>
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                        <Row>
                            <Col xs='6'>
                                <Row className='form-group'>
                                    <Col xs='4' className='p-2'>
                                        <Label>Salad: </Label>
                                    </Col>
                                    <Col xs='8'>
                                        <Input className='form-control' disabled value={data.salad}></Input>
                                    </Col>
                                </Row>
                            </Col>
                            <Col xs='6'>
                                <Row className='form-group'>
                                    <Col xs='4' className='p-2'>
                                        <Label>Số phone: <span>(*)</span></Label>
                                    </Col>
                                    <Col xs='8'>
                                        <Input className='form-control' type='number' value={data.soDienThoai}></Input>
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                        <Row>
                            <Col xs='6'>
                                <Row className='form-group'>
                                    <Col xs='4' className='p-2'>
                                        <Label>SL Nước: </Label>
                                    </Col>
                                    <Col xs='8'>
                                        <Input className='form-control' disabled value={data.soLuongNuoc}></Input>
                                    </Col>
                                </Row>
                            </Col>
                            <Col xs='6'>
                                <Row className='form-group'>
                                    <Col xs='4' className='p-2'>
                                        <Label>Địa chỉ: <span>(*)</span></Label>
                                    </Col>
                                    <Col xs='8'>
                                        <Input className='form-control' value={data.diaChi}></Input>
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                        <Row>
                            <Col xs='6'>
                                <Row className='form-group'>
                                    <Col xs='4' className='p-2'>
                                        <Label>Thành tiền: </Label>
                                    </Col>
                                    <Col xs='8'>
                                        <Input className='form-control' disabled value={data.thanhTien}></Input>
                                    </Col>
                                </Row>
                            </Col>
                            <Col xs='6'>
                                <Row className='form-group'>
                                    <Col xs='4' className='p-2'>
                                        <Label>Lời nhắn:</Label>
                                    </Col>
                                    <Col xs='8'>
                                        <Input className='form-control' value={data.loiNhan}></Input>
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                        <Row>
                            <Col xs='6'>
                                <Row className='form-group'>
                                    <Col xs='4' className='p-2'>
                                        <Label>Loại Pizza: <span>(*)</span></Label>
                                    </Col>
                                    <Col xs='8'>
                                        <select className='form-control'>
                                            <option >{data.loaiPizza}</option>
                                        </select>
                                    </Col>
                                </Row>
                            </Col>
                            <Col xs='6'>
                                <Row className='form-group'>
                                    <Col xs='4' className='p-2'>
                                        <Label>Trạng thái: <span>(*)</span></Label>
                                    </Col>
                                    <Col xs='8'>
                                        <select className='form-control' onChange={onStatusChange}>
                                            <option value='open'>Open</option>
                                            <option value='cancel'>Đã hủy</option>
                                            <option value='confirmed'>Đã xác nhận</option>
                                        </select>
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                        <Row>
                            <Col xs='6'>
                                <Row className='form-group'>
                                    <Col xs='4' className='p-2'>
                                        <Label>Mã Voucher:</Label>
                                    </Col>
                                    <Col xs='8'>
                                        <Input className='form-control' value={data.idVourcher}></Input>
                                    </Col>
                                </Row>
                            </Col>
                            <Col xs='6'>
                                <p className='text-primary'>Chỉ cho thay đổi trạng thái đơn hàng. Các thông tin khác không cho phép sửa!.</p>
                            </Col>
                        </Row>
                    </ModalBody>
                    <ModalFooter>
                        <Button
                            color="success"
                            onClick={onBtnConfirmClk}
                        >
                            Update
                        </Button>
                        {' '}
                        <Button onClick={handleClose} color='warning'>
                            Cancel
                        </Button>
                    </ModalFooter>
                </Box>
            </Modal>
            <AlertModal openAlertModal={alertModal} setOpenAlertModal={setAlertModal} message={message}/>
        </div>
    )
}
export default UpdateModal;