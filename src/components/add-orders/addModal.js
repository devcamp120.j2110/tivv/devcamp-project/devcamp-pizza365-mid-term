import { Modal, Box } from '@mui/material';
import { useState, useEffect } from 'react';
import { ModalHeader, ModalBody, ModalFooter, Button, Input, Col, Row, Label } from 'reactstrap'
import AlertModal from '../alert/alertModal';
const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 800,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};
function AddModal({ openModal, setOpenModal }) {
    //Đóng modal
    const handleClose = () => {
        setOpenModal(false);
        setKichCo("0");
        window.location.reload();
    }
    //Biến lưu danh sách drink
    const [drinks, setDrinks] = useState([])
    //Hàm gọi API lấy danh sách drink
    const getDrink = async () => {
        const Fetch = await fetch("http://42.115.221.44:8080/devcamp-pizza365/drinks");
        const response = await Fetch.json();
        return response;
    }
    //Hàm gọi API tạo đơn hàng
    const createOrder = async (paramUrl, paramOption = {}) => {
        const Fetch = await fetch(paramUrl, paramOption);
        const response = await Fetch.json();
        return response;
    }
    //Hàm kiểm tra voucher
    const checkVoucher = async (voucher) => {
        const Fetch = await fetch("http://42.115.221.44:8080/devcamp-voucher-api/voucher_detail/" + voucher);
        const response = await Fetch.json();
        return response;
    }
    //Lấy danh sách drink khi modal được mở
    useEffect(() => {
        getDrink().then((response) => {
            setDrinks(response);
        }).catch(() => {
            setMessage("Không lấy được danh sách đồ uống!");
            setAlertModal(true);
        });
    }, []);
    //Khai báo biến lưu trữ thông tin 
    const [kichCo, setKichCo] = useState("0");
    const [duongKinh, setDuongKinh] = useState("");
    const [suonNuong, setSuonNuong] = useState("");
    const [salad, setSalad] = useState("");
    const [soLuongNuoc, setSoLuongNuoc] = useState("");
    const [thanhTien, setThanhTien] = useState("");
    const [loaiPizza, setLoaiPizza] = useState("0");
    const [voucher, setVoucher] = useState("");
    const [nuocUong, setNuocUong] = useState("0");
    const [hoten, setHoTen] = useState("");
    const [email, setEmail] = useState("");
    const [soDienThoai, setSoDienThoai] = useState("");
    const [diaChi, setDiaChi] = useState("");
    const [loiNhan, setLoiNhan] = useState("");
    //Biến lưu trạng thái đóng, mở modal alert
    const [alertModal, setAlertModal] = useState(false);
    //Biến lưu thông tin hiển thị lên Popup
    const [message, setMessage] = useState("");
    //Hàm xử lý khi chọn kích cỡ Pizza
    const onSelectComboSizeChange = (event) => {
        setKichCo(event.target.value);
    }
    useEffect(() => {
        if (kichCo === 'S') {
            setDuongKinh("20");
            setSoLuongNuoc("2");
            setSuonNuong("2");
            setSalad("200");
            setThanhTien("150000");
        };
        if (kichCo === 'M') {
            setDuongKinh("25");
            setSoLuongNuoc("3");
            setSuonNuong("4");
            setSalad("300");
            setThanhTien("200000");
        };
        if (kichCo === 'L') {
            setDuongKinh("30");
            setSoLuongNuoc("4");
            setSuonNuong("8");
            setSalad("500");
            setThanhTien("250000");
        };
        if (kichCo === '0') {
            setDuongKinh("");
            setSoLuongNuoc("");
            setSuonNuong("");
            setSalad("");
            setThanhTien("");
        };
    }, [kichCo]);
    //lưu dữ liệu vào biến khi chọn nước uống
    const onDrinkChange = (event) => {
        setNuocUong(event.target.value);
    }
    //lưu dữ liệu vào biến khi chọn loại Pizza
    const onPizzaChange = (event) => {
        setLoaiPizza(event.target.value);
    }
    //lưu dữ liệu vào biến khi nhập lời nhắn
    const onMessageChange = (event) => {
        setLoiNhan(event.target.value);
    }
    //lưu dữ liệu vào biến khi nhập Voucher
    const onVoucherChange = (event) => {
        setVoucher(event.target.value);
    }
    //lưu dữ liệu vào biến khi nhập họ tên
    const onNameChange = (event) => {
        setHoTen(event.target.value);
    }
    //lưu dữ liệu vào biến khi nhập email
    const onEmailChange = (event) => {
        setEmail(event.target.value);
    }
    //lưu dữ liệu vào biến khi nhập số điện thoại
    const onNumberPhoneChange = (event) => {
        setSoDienThoai(event.target.value);
    }
    //lưu dữ liệu vào biến khi nhập địa chỉ
    const onAddressChange = (event) => {
        setDiaChi(event.target.value);
    }
    //Hàm kiểm tra dữ liệu input
    const validateData = () => {
        if (kichCo === '0') {
            setMessage("Vui lòng chọn kích cỡ Pizza!");
            setAlertModal(true);
            return false;
        }
        if (loaiPizza === '0') {
            setMessage("Vui lòng chọn loại Pizza!");
            setAlertModal(true);
            return false;
        }
        if (isNaN(voucher.trim()) && voucher.trim() !== '') {
            setMessage("Mã Voucher phải là số!");
            setAlertModal(true);
            return false;
        }
        if (nuocUong === '0') {
            setMessage("Vui lòng chọn loại nước uống!");
            setAlertModal(true);
            return false;
        }
        if (hoten.trim() === '') {
            setMessage("Vui lòng nhập họ tên!");
            setAlertModal(true);
            return false;
        }

        if (email.trim() !== '') {
            var re = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
            if (!re.test(email.trim())) {
                setMessage("Email nhập sai định dạng!");
                setAlertModal(true);
                return false;
            }
        }
        if (soDienThoai.trim() === '') {
            setMessage("Vui lòng nhập số điện thoại!");
            setAlertModal(true);
            return false;
        }
        if (diaChi.trim() === '') {
            setMessage("Vui lòng nhập địa chỉ!");
            setAlertModal(true);
            return false;
        }
        return true;
    }
    //Hàm xử lý nút xác nhận đặt hàng
    const onBtnConfirmClk = () => {
        if (validateData()) {
            createOrder("http://42.115.221.44:8080/devcamp-pizza365/orders", {
                method: 'POST',
                body: JSON.stringify({
                    kichCo: kichCo,
                    duongKinh: duongKinh,
                    suon: suonNuong,
                    salad: salad,
                    loaiPizza: loaiPizza,
                    idVourcher: voucher,
                    idLoaiNuocUong: nuocUong,
                    soLuongNuoc: soLuongNuoc,
                    hoTen: hoten,
                    thanhTien: thanhTien,
                    email: email,
                    soDienThoai: soDienThoai,
                    diaChi: diaChi,
                    loiNhan: loiNhan
                }),
                headers: {
                    'Content-type': 'application/json; charset=UTF-8'
                }
            })
                .then((data) => {
                    checkVoucher(voucher).then((response) => {
                        if (response.phanTramGiamGia<0 || response.phanTramGiamGia >100)
                        {
                            setMessage(`Đơn hàng được tạo thành công! Mã đơn hàng của bạn là: ${data.orderId}\n
                    Đơn giá: ${thanhTien} VNĐ, Mã giảm giá: ${voucher}, phần trăm giảm giá: 0% \n
                     Thành tiền: ${thanhTien} VNĐ.`);
                        } else
                        setMessage(`Đơn hàng được tạo thành công! Mã đơn hàng của bạn là: ${data.orderId}\n
                    Đơn giá: ${thanhTien} VNĐ, Mã giảm giá: ${voucher}, phần trăm giảm giá: ${response.phanTramGiamGia}% \n
                     Thành tiền: ${thanhTien - thanhTien*response.phanTramGiamGia/100} VNĐ.`);
                    }).catch(()=> {
                        setMessage(`Đơn hàng được tạo thành công! Mã đơn hàng của bạn là: ${data.orderId}\n
                    Đơn giá: ${thanhTien} VNĐ, Mã giảm giá: ${voucher}, phần trăm giảm giá: 0% \n
                     Thành tiền: ${thanhTien} VNĐ.`);
                    });
                    setAlertModal(true);
                })
                .catch(() => {
                    setMessage("Tạo đơn hàng bị lỗi!");
                    setAlertModal(true);
                });
        }
    }

    return (
        <div>
            <Modal
                open={openModal}
                onClose={handleClose}
            >
                <Box sx={style}>
                    <ModalHeader className='pt-0'>
                        Order Information
                    </ModalHeader>
                    <ModalBody>
                        <Row>
                            <Col xs='6'>
                                <Row className='form-group'>
                                    <Col xs='4' className='p-2'>
                                        <Label>Kích cỡ: <span>(*)</span></Label>
                                    </Col>
                                    <Col xs='8'>
                                        <select className='form-control' onChange={onSelectComboSizeChange}>
                                            <option value='0'>---Chọn kích cỡ Combo---</option>
                                            <option value='S'>S</option>
                                            <option value='M'>M</option>
                                            <option value='L'>L</option>
                                        </select>
                                    </Col>
                                </Row>
                            </Col>
                            <Col xs='6'>
                                <Row className='form-group'>
                                    <Col xs='4' className='p-2'>
                                        <Label>Nước uống: <span>(*)</span></Label>
                                    </Col>
                                    <Col xs='8'>
                                        <select className='form-control' onChange={onDrinkChange}>
                                            <option value='0'>---Chọn loại nước uống---</option>
                                            {drinks.map((elements, index) => (
                                                <option key={index + 1}>{elements.maNuocUong}</option>
                                            ))}
                                        </select>
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                        <Row>
                            <Col xs='6'>
                                <Row className='form-group'>
                                    <Col xs='4' className='p-2'>
                                        <Label>Đường kính: </Label>
                                    </Col>
                                    <Col xs='8'>
                                        <Input className='form-control' placeholder='Chọn kích cỡ combo' disabled defaultValue={duongKinh}></Input>
                                    </Col>
                                </Row>
                            </Col>
                            <Col xs='6'>
                                <Row className='form-group'>
                                    <Col xs='4' className='p-2'>
                                        <Label>Họ tên: <span>(*)</span></Label>
                                    </Col>
                                    <Col xs='8'>
                                        <Input className='form-control' placeholder='Nhập họ và tên' onChange={onNameChange}></Input>
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                        <Row>
                            <Col xs='6'>
                                <Row className='form-group'>
                                    <Col xs='4' className='p-2'>
                                        <Label>Sườn nướng: </Label>
                                    </Col>
                                    <Col xs='8'>
                                        <Input className='form-control' placeholder='Chọn kích cỡ combo' disabled defaultValue={suonNuong}></Input>
                                    </Col>
                                </Row>
                            </Col>
                            <Col xs='6'>
                                <Row className='form-group'>
                                    <Col xs='4' className='p-2'>
                                        <Label>Email:</Label>
                                    </Col>
                                    <Col xs='8'>
                                        <Input className='form-control' placeholder='Nhập email' onChange={onEmailChange}></Input>
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                        <Row>
                            <Col xs='6'>
                                <Row className='form-group'>
                                    <Col xs='4' className='p-2'>
                                        <Label>Salad: </Label>
                                    </Col>
                                    <Col xs='8'>
                                        <Input className='form-control' placeholder='Chọn kích cỡ combo' disabled defaultValue={salad}></Input>
                                    </Col>
                                </Row>
                            </Col>
                            <Col xs='6'>
                                <Row className='form-group'>
                                    <Col xs='4' className='p-2'>
                                        <Label>Số phone: <span>(*)</span></Label>
                                    </Col>
                                    <Col xs='8'>
                                        <Input className='form-control' type='number' placeholder='Nhập số điện thoại' onChange={onNumberPhoneChange}></Input>
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                        <Row>
                            <Col xs='6'>
                                <Row className='form-group'>
                                    <Col xs='4' className='p-2'>
                                        <Label>SL Nước: </Label>
                                    </Col>
                                    <Col xs='8'>
                                        <Input className='form-control' placeholder='Chọn kích cỡ combo' disabled defaultValue={soLuongNuoc}></Input>
                                    </Col>
                                </Row>
                            </Col>
                            <Col xs='6'>
                                <Row className='form-group'>
                                    <Col xs='4' className='p-2'>
                                        <Label>Địa chỉ: <span>(*)</span></Label>
                                    </Col>
                                    <Col xs='8'>
                                        <Input className='form-control' placeholder='Nhập địa chỉ' onChange={onAddressChange}></Input>
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                        <Row>
                            <Col xs='6'>
                                <Row className='form-group'>
                                    <Col xs='4' className='p-2'>
                                        <Label>Thành tiền: </Label>
                                    </Col>
                                    <Col xs='8'>
                                        <Input className='form-control' placeholder='Chọn kích cỡ combo' disabled defaultValue={thanhTien}></Input>
                                    </Col>
                                </Row>
                            </Col>
                            <Col xs='6'>
                                <Row className='form-group'>
                                    <Col xs='4' className='p-2'>
                                        <Label>Lời nhắn:</Label>
                                    </Col>
                                    <Col xs='8'>
                                        <Input className='form-control' placeholder='Nhập lời nhắn' onChange={onMessageChange}></Input>
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                        <Row>
                            <Col xs='6'>
                                <Row className='form-group'>
                                    <Col xs='4' className='p-2'>
                                        <Label>Loại Pizza: <span>(*)</span></Label>
                                    </Col>
                                    <Col xs='8'>
                                        <select className='form-control' onChange={onPizzaChange}>
                                            <option value='0'>---Chọn loại Pizza---</option>
                                            <option value='Seafood'>Hải sản</option>
                                            <option value='Hawaii'>Hawaii</option>
                                            <option value='Bacon'>Thịt hun khói</option>
                                        </select>
                                    </Col>
                                </Row>
                            </Col>
                            <Col xs='6'>
                                <Row className='form-group'>
                                    <Col xs='4' className='p-2'>
                                        <Label>Trạng thái: <span>(*)</span></Label>
                                    </Col>
                                    <Col xs='8'>
                                        <select className='form-control' disabled>
                                            <option value='open'>Open</option>
                                        </select>
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                        <Row>
                            <Col xs='6'>
                                <Row className='form-group'>
                                    <Col xs='4' className='p-2'>
                                        <Label>Mã Voucher:</Label>
                                    </Col>
                                    <Col xs='8'>
                                        <Input className='form-control' placeholder='Nhập mã giảm giá' onChange={onVoucherChange}></Input>
                                    </Col>
                                </Row>
                            </Col>
                            <Col xs='6'>
                                <p className='pt-2 font-italic text-primary'>Các trường có (*) bắt buộc phải nhập.</p>
                            </Col>
                        </Row>
                    </ModalBody>
                    <ModalFooter>
                        <Button
                            color="success"
                            onClick={onBtnConfirmClk}
                        >
                            Đặt hàng
                        </Button>
                        {' '}
                        <Button onClick={handleClose} color='warning'>
                            Cancel
                        </Button>
                    </ModalFooter>
                </Box>
            </Modal>
            <AlertModal openAlertModal={alertModal} setOpenAlertModal={setAlertModal} message={message} />
        </div>
    )
}
export default AddModal;