import { Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions, Button } from '@mui/material'
import { useState } from 'react';
import AlertModal from '../alert/alertModal';
function DeleteModal({openModal, setOpenModal, data}) {
    const handleClose = () => {
        setOpenModal(false);
    };
    //Biến lưu trạng thái đóng, mở modal alert
    const [alertModal, setAlertModal] = useState(false);
    //Biến lưu thông tin hiển thị lên Popup
    const [message, setMessage] = useState("");
    //Hàm gọi API delete đơn hàng
    const deleteOrder = async (paramUrl, paramOption = {}) => {
        const Fetch = await fetch(paramUrl, paramOption);
        return Fetch;
    }
    const handleDelete = () => {
        deleteOrder("http://42.115.221.44:8080/devcamp-pizza365/orders/" + data.id, {
                method: 'DELETE',
            })
                .then(() => {
                    setMessage("Xóa đơn hàng thành công!");
                    setAlertModal(true);
                    window.location.reload();
                })
                .catch((error) => {
                    setMessage("Xóa đơn hàng bị lỗi!");
                    setAlertModal(true);
                    console.log(error);
                });
    }
    return (
        <div>
            <Dialog
            open={openModal}
            onClose={handleClose}
        >
            <DialogTitle>
                Thông báo
            </DialogTitle>
            <DialogContent>
                <DialogContentText>
                    Bạn có chắc chắn muốn xóa đơn hàng?
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button onClick={handleDelete}>Delete</Button>
                <Button onClick={handleClose}>Cancel</Button>
            </DialogActions>
        </Dialog>
        <AlertModal openAlertModal={alertModal} setOpenAlertModal={setAlertModal} message={message}/>
        </div>
    )
}
export default DeleteModal;