import { Button, Col, Container, Label, Row, Table } from 'reactstrap';
import 'bootstrap/dist/css/bootstrap.min.css'
import { useEffect, useState } from 'react';
import AddModal from './components/add-orders/addModal';
import UpdateModal from './components/update-orders/updateModal';
import AlertModal from './components/alert/alertModal';
import DeleteModal from './components/delete-orders/deleteModal';

function DataTable() {
    //Biến lưu toàn bộ danh sách order gốc
    const [orders, setOrders] = useState([]);
    //Biến lưu danh sách dùng để hiển thị
    const [ordersDisplay, setOrdersDisplay] = useState([]);
    //Biến lưu loại pizza và trạng thái trong điều kiện lọc
    const [pizzaType, setPizzaType] = useState("All");
    const [status, setStatus] = useState("All");
    //Biến lưu dữ liệu được tìm thấy trong điều kiện lọc
    const [ordersFilt, setOrdersFilt] = useState([]);
    //Biến lưu trạng thái đóng, mở modal alert
    const [alertModal, setAlertModal] = useState(false);
    //Biến lưu thông tin hiển thị lên Popup
    const [message, setMessage] = useState("");
    //Biến Cấu hình add modal
    const [addModal, setAddModal] = useState(false);
    //Biến Cấu hình update modal
    const [updateModal, setUpdateModal] = useState(false);
    //Biến Cấu hình delete modal
    const [deleteModal, setDeleteModal] = useState(false);
    //Biến lưu dữ liệu của dòng được chọn
    const [data, setData] = useState([]);
    //Hàm gọi API lấy danh sách order
    const CallAPI = async () => {
        const Fetch = await fetch("http://42.115.221.44:8080/devcamp-pizza365/orders");
        const response = await Fetch.json();
        return response;
    }
    //Lấy danh sách order khi tải trang
    useEffect(() => {
        CallAPI().then((response) => {
            setOrders(response);
            setOrdersDisplay(response);
        }).catch(() => {
            setAlertModal(true);
            setMessage("Tải danh sách bị lỗi. Vui lòng reload lại trang web!");
        });
    }, []);
    //Hàm xử lý sự kiện thêm mới đơn hàng
    const AddNewOrder = () => {
        setAddModal(true);
    }
    //Hàm xử lý sự kiện sửa thông tin đơn hàng
    const Modify = (data) => {
        setUpdateModal(true);
        setData(data);
        
    }
    //Hàm xử lý sự kiện xóa đơn hàng
    const Delete = (data) => {
        setData(data);
        setDeleteModal(true);
    }
    //Hàm xử lý sự kiện lọc dữ liệu
    const FilterData = () => {
        if (pizzaType === "All" && status !== "All") {
            orders.forEach(element => {
                if (element.trangThai === status) {
                    setOrdersFilt(ordersFilt.push(element));
                }
            });
            setOrdersDisplay(ordersFilt);
        };
        if (pizzaType !== "All" && status === "All") {
            orders.forEach(element => {
                if (element.loaiPizza === pizzaType) {
                    setOrdersFilt(ordersFilt.push(element));
                }
            });
            setOrdersDisplay(ordersFilt);
        };
        if (pizzaType !== "All" && status !== "All") {
            orders.forEach(element => {
                if (element.loaiPizza === pizzaType && element.trangThai === status) {
                    setOrdersFilt(ordersFilt.push(element));
                }
            });
            setOrdersDisplay(ordersFilt);
        };
        if (pizzaType === "All" && status === "All") {
            setOrdersDisplay(orders);
        }
    }
    useEffect(() => {
        setOrdersFilt([]);
    }, [pizzaType, status])
    //Hàm lấy giá trị khi Pizza Type được chọn
    const getPizzaType = (event) => {
        setPizzaType(event.target.value);
    }
    //Hàm lấy giá trị khi Status được chọn
    const getStatus = (event) => {
        setStatus(event.target.value);
    }
    /////////////

    return (
        <Container>
            <div className='bg-light fixed-top' >
            <p className='h3 text-center py-2'>Quản lý đơn hàng</p>
                <div className='container m-auto'>
                    <Row>
                        <Col xs='5'>
                            <Row>
                                <Col xs='3'>
                                    <Label className='pt-2'>Pizza type: </Label>
                                </Col>
                                <Col xs='9'>
                                    <select className='form-control' onChange={getPizzaType}>
                                        <option value='All'>All</option>
                                        <option value='Seafood'>Hải sản</option>
                                        <option value='Hawaii'>Hawaii</option>
                                        <option value='Bacon'>Thịt hun khói</option>
                                    </select>
                                </Col>
                            </Row>
                        </Col>
                        <Col xs='5'>
                            <Row>
                                <Col xs='3'>
                                    <Label className='pt-2'>Status: </Label>
                                </Col>
                                <Col xs='9'>
                                    <select className='form-control' onChange={getStatus}>
                                        <option value='All'>All</option>
                                        <option value='open'>Open</option>
                                        <option value='cancel'>Đã hủy</option>
                                        <option value='confirmed'>Đã xác nhận</option>
                                    </select>
                                </Col>
                            </Row>
                        </Col>
                        <Col xs='2'>
                            <Button className='btn btn-info' onClick={FilterData}>Filter</Button>
                        </Col>
                    </Row>
                    <Button className='btn btn-success my-2' onClick={AddNewOrder}><i className="fa-solid fa-plus"></i> Add New Order</Button>
                </div> 
            </div>
            <Table className='table table-striped table-bordered' style={{marginTop:'150px'}}>
                <thead className='bg-primary'>
                    <tr className='text-center text-white'>
                        <td>STT</td>
                        <td>Order ID</td>
                        <td>Combo Size</td>
                        <td>Pizza Type</td>
                        <td>Drink</td>
                        <td>Full Name</td>
                        <td>Number Phone</td>
                        <td>Address</td>
                        <td>Status</td>
                        <td>Action</td>
                    </tr>
                </thead>
                <tbody>
                    {ordersDisplay.map((element, index) => (
                        <tr key={index}>
                            <td>{index + 1}</td>
                            <td>{element.id}</td>
                            <td>{element.kichCo}</td>
                            <td>{element.loaiPizza}</td>
                            <td>{element.idLoaiNuocUong}</td>
                            <td>{element.hoTen}</td>
                            <td>{element.soDienThoai}</td>
                            <td>{element.diaChi}</td>
                            <td>{element.trangThai}</td>
                            <td>
                                <i className="fa-solid fa-pen-to-square px-2 text-primary" onClick={() => Modify(element)}></i>
                                <i className="fa-solid fa-trash-can px-2 text-danger" onClick={() => Delete(element)}></i>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </Table>
           
            <AddModal openModal={addModal} setOpenModal={setAddModal}/>
            <UpdateModal openModal={updateModal} setOpenModal={setUpdateModal} data={data}/>
            <DeleteModal openModal={deleteModal} setOpenModal={setDeleteModal} data={data}/>
            <AlertModal openAlertModal={alertModal} setOpenAlertModal={setAlertModal} message={message}/>
        </Container>
    )
}
export default DataTable;